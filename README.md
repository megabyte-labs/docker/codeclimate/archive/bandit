# Code Climate Bandit Engine

Code Climate Engine to run [Bandit](https://github.com/PyCQA/bandit).

Bandit is a tool designed to find common security issues in Python code.


## Usage

.codeclimate.yml
```
plugins:
  bandit:
    enabled: true
    config:
      python_version: 3
```

Only Python `3` is currently supported.

**NOTE**: If you don't set a Python version, Python 3 will be used by default.

```
codeclimate analyze
```

## Configuration

The engine supports the native config file for Bandit. You can select the specific test plugins to run and override default Bandit configuration using this file. More information on the config file can be found in the [Bandit documentation](https://docs.openstack.org/bandit/latest/config.html).

A `.bandit.yaml` included at the root of your project will be included during engine run.

Example `.bandit.yaml`:

```
skips: ['B101', 'B601', 'B404']
```
## ➤ Requirements

- **[Docker](https://gitlab.com/megabyte-labs/ansible-roles/docker)**
- [CodeClimate CLI](https://github.com/codeclimate/codeclimate)

### Optional Requirements

- [DockerSlim](https://gitlab.com/megabyte-labs/ansible-roles/dockerslim) - Used for generating compact, secure images
- [Google's Container structure test](https://github.com/GoogleContainerTools/container-structure-test) - For testing the Docker images

## ➤ Example Usage

The Code Climate engine built using this repository can be used for analysis using code climate cli. 

```shell
codeclimate analyze
```

This allows you to run code climate analysis from the root of  your project directory. Ensure `.codeclimate.yml` file is present on the root of your project directory. A sample configuration of this file  is present in this repository.Once the analysis is complete it will display the results in code climate format.

If you have created the docker image locally and wish to test it you can do so using below command

```shell
codeclimate analyze --dev
```
In order to run slim docker image of this engine , please pull the latest slim docker image locally ( or create one ) and retag it to latest before running the same.



### Building the Docker Container

Run the below make command from the root of this repository to create a local fat docker image
```shell
make image
```

### Building a Slim Container

Run the below make command from the root of this repository to create a local slim docker image
```shell
make slim
```


### Test

Run the below command from the root of this repository to test the images created by this repository.
```shell
make test
```




## TODO

- support different locations of .bandit.yaml
