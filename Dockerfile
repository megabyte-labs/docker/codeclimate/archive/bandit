# Changing this as pip dependency is tied to alpine version
# Latest Alpine does not seem to support python2, so removing 
# the same from install
FROM alpine:3.15

LABEL maintainer "Benji Visser <benny@noqcks.io>"

WORKDIR /usr/src/app/

COPY requirements.txt /usr/src/app/

RUN apk --update add  \
  python3 py-pip && \
  pip install --upgrade pip && \
  pip install -r requirements.txt && \
  mv /usr/bin/bandit /usr/bin/bandit3 && \
  rm /var/cache/apk/*

COPY . /usr/src/app

RUN adduser -u 9000 app -D
USER app

VOLUME /code
WORKDIR /code

CMD ["python3", "/usr/src/app/run.py"]


ARG BUILD_DATE
ARG REVISION
ARG VERSION

LABEL maintainer="Megabyte Labs <help@megabyte.space>"
LABEL org.opencontainers.image.authors="Brian Zalewski <brian@megabyte.space>"
LABEL org.opencontainers.image.created=$BUILD_DATE
LABEL org.opencontainers.image.description="Code Climate engine for Bandit"
LABEL org.opencontainers.image.documentation="https://gitlab.com/megabyte-labs/docker/codeclimate/bandit/-/blob/master/README.md"
LABEL org.opencontainers.image.licenses="MIT"
LABEL org.opencontainers.image.revision=$REVISION
LABEL org.opencontainers.image.source="https://gitlab.com/megabyte-labs/docker/codeclimate/bandit.git"
LABEL org.opencontainers.image.url="https://megabyte.space"
LABEL org.opencontainers.image.vendor="Megabyte Labs"
LABEL org.opencontainers.image.version=$VERSION
LABEL space.megabyte.type="code-climate"
