IMAGE_NAME ?= codeclimate/codeclimate-bandit

SLIM_IMAGE_NAME ?= codeclimate/codeclimate-bandit:slim

.PHONY: release

release:
	docker build --tag $(IMAGE_NAME) .

image:
	docker build --tag $(IMAGE_NAME) .

slim: image
	docker-slim build --tag $(SLIM_IMAGE_NAME) --http-probe=false --exec 'bandit3 -f json -r .  || continue' --mount "$$PWD/tests/example:/work" --workdir '/work' --preserve-path '/usr/lib/python3.9/site-packages/bandit' --preserve-path '/usr/bin/bandit3' $(IMAGE_NAME) && prettier --write slim.report.json 

test: slim
	container-structure-test test --image $(IMAGE_NAME) --config tests/container-test-config.yaml && container-structure-test test --image $(SLIM_IMAGE_NAME) --config tests/container-test-config.yaml
